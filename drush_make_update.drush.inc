<?php
/**
 * @file
 * The Drush make update extension.
 */

/**
 * Implements hook_drush_command().
 */
function drush_make_update_drush_command() {
  $items = array();

  $items['make-update'] = array(
    'description' => "Checks a make file for more up-to-date projects",
    'arguments' => array(
      'make file' => dt('The location of a make file to check'),
    ),
    'options' => array(
    ),
    'drush dependencies' => array('drush_make'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  return $items;
}

/**
 *
 */
function drush_drush_make_update_make_update_validate() {
  $args = func_get_args();
  if (empty($args)) {
    return drush_set_error('DRUSH_MAKE_UPDATE_INCORRECT_ARGUMENTS', dt("You must specify the makefile to scan."));
  }
}

/**
 * The main drush remake command callback.
 */
function drush_drush_make_update_make_update($makefile) {
  $info = drush_make_parse_info_file($makefile);
  
  // Support making just a portion of a make file
  $restrictions = array(
    'projects' => drush_get_option('projects', FALSE),
    'libraries' => drush_get_option('libraries', FALSE),
  );
  if (count(array_filter($restrictions))) {
    foreach ($restrictions as $type => $keys) {
      $keys = trim($keys);
      if ($keys !== '*') {
        if (!empty($keys)) {
          $info[$type] = array_intersect_key($info[$type], array_fill_keys(explode(',', $keys), 1));
        } else {
          $info[$type] = array();
        }
      }
    }
  }

  if ($info === FALSE || ($info = drush_make_validate_info_file($info)) === FALSE) {
    return FALSE;
  }
  
  drush_make_update_drush_make_projects($info);
  
}

function drush_make_update_drush_make_projects($info) {
  
  // Swap out the logging callback so we don't log all the data.
  $logging_callback = drush_get_context('DRUSH_LOG_CALLBACK', '_drush_print_log');
  drush_set_context('DRUSH_LOG_CALLBACK', 'drush_make_update_drush_print_log');
  
  $projects = array();

  foreach ($info['projects'] as $key => $project) {
    $md5 = '';
    if (isset($project['md5'])) {
      $md5 = $project['md5'];
    }
    // Merge the known data onto the project info.
    $project += array(
      'name'                => $key,
      'core'                => $info['core'],
      'version'             => DRUSH_MAKE_VERSION_BEST,
      'location'            => drush_get_option('drush-make-update-default-url'),
    );
    if ($project['location'] != 'http://updates.drupal.org/release-history' && !isset($project['type'])) {
      $project = drush_make_updatexml($project);
      $project['download_type'] = ($project['type'] == 'core' ? 'core' : 'contrib');
    }
    elseif (!empty($project['type'])) {
      $project['download_type'] = ($project['type'] == 'core' ? 'core' : 'contrib');
    }
    else {
      $project['download_type'] = ($project['name'] == 'drupal' ? 'core' : 'contrib');
    }
    $projects[$project['download_type']][$project['name']] = $project;
  }

  $cores = !empty($projects['core']) ? count($projects['core']) : 0;

  foreach ($projects as $type => $type_projects) {
    foreach ($type_projects as $project) {
      if ($project['location'] == 'http://updates.drupal.org/release-history' && (!isset($project['type']) || !isset($project['download']))) {
        $project = drush_make_updatexml($project);
      }
      if ($instance = DrushMakeProject::getInstance($project['type'], $project)) {
        $projects[($project['type'] == 'core' ? 'core' : 'contrib')][$project['name']] = $instance;
      }
      else {
        drush_make_error('PROJECT-TYPE', dt('Non-existent project type %type on project %project', array('%type' => $project['type'], '%project' => $key)));
      }
    }
  }

  if (isset($projects['core'])) {
    foreach ($projects['core'] as $project) {
      
      //$project->make();
    }
  }
  
  // Set the logging back
  drush_set_context('DRUSH_LOG_CALLBACK', $logging_callback);

  $results[] = array(
    dt('Project'),
    dt('Current version'),
    dt('Recommeded version'),
    dt('Patches'),
  );
  
  if (isset($projects['contrib'])) {
    foreach ($projects['contrib'] as $project) {
      //$project->make();
      if ($result = drush_make_update_drush_make_update_xml_download($project)) {
        $results[] = $result;
      }
    }
  }
  
  drush_print_table($results);
  
  
  return TRUE;
}

/**
 * Dummy log function, that doesn't log.
 */
function drush_make_update_drush_print_log() {
  return TRUE;
}

function drush_make_update_drush_make_update_xml_download($project) {
  if (!isset($project->release_history)) {
    return;
  }
  // Make an array of releases.
  foreach ($project->release_history->releases->release as $release) {
    $version = (string) $release->version_major;
    // there should be version_patch attribute for every stable release
    // so checking whether the attribute exists should be enough
    if (isset($release->version_patch) && ((string) $release->version_extra) != 'dev') {
      $version .= '.' . (string) $release->version_patch;
    }
    // if version_patch attribute does not exist, then it should be a dev release
    // and the version string should be in format MAJOR_VERSION.x-dev
    else {
      $version .= '.x';
    }

    if ($extra_version = (string) $release->version_extra) {
      $version .= '-' . $extra_version;
    }

    $releases[$version] = array(
      'file' => (string) $release->download_link,
      'md5' => (string) $release->mdhash,
      'version' => (string) $release->version,
    );
    foreach (array('major', 'patch', 'extra') as $part) {
      $releases[$version][$part] = (string) $release->{'version_' . $part};
    }
    if ($releases[$version]['extra'] == 'dev') {
      $releases[$version]['patch'] = 'x';
    }
  }

  // Find the best release.
  $recommended_major = (string)$project->release_history->recommended_major;
  if (!$recommended_major && $project->type == 'core') {
    $recommended_major = strtok($project->core, '.');
  }
  $best_version = _drush_make_update_xml_best_version($recommended_major, $releases);
  if ($best_version != $project->version) {
    return array(
      'name' => $project->name,
      'current_version' => $project->version,
      'best_version' => $best_version,
      'patches' => isset($project->patch) ? count($project->patch) : 0,
    );
  }
}
